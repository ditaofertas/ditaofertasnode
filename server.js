import path from 'path';
import app from './src/app';

app.set('port', (process.env.PORT || 3000));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.listen(app.get('port'), () => {
  console.log('Node app is running on port', app.get('port'));
});
