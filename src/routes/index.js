import express from 'express';
import path from 'path';
import dbConnection from './../config/database';
import OffersModel from './../models/Offers';
import CategoriesModel from './../models/Categories';
import ApplicationController from './../controllers/Application';
const router = express.Router();

const getAppCtrl = () => {
  const connection = dbConnection();
  return new ApplicationController(connection, CategoriesModel, OffersModel);
};

router.get('/', (request, response) => {
  /*response.render('index', (err, html) => {
    console.error('err', err);
  });*/
  response.sendFile('index.html', {
    root: './views/'
  }, (err) => {
    console.error('err', err);
  });
});

/**
 * Minera as categorias das lojas e salva no banco
 */
router.get('/saveCategories', (request, response) => {
  const appCtrl = getAppCtrl();
  appCtrl.saveCategories(request, response);
});

/**
 * Salva no banco o relacionamento entre as categorias 'default' da aplicação e as mineradas nas lojas
 * - Deve ser executado depois que as categorias foram mineradas
 */
router.get('/saveDefaultCategories', (request, response) => {
  const appCtrl = getAppCtrl();
  appCtrl.saveDefaultCategories(request, response);
});

/**
 * Minera as ofertas de todas as categorias padrões e salva no banco
 */
router.get('/saveOffers', (request, response) => {
  const appCtrl = getAppCtrl();
  appCtrl.saveOffers(request, response);
});

router.get('/getOffers', (request, response) => {
  const appCtrl = getAppCtrl();
  appCtrl.getOffers(request, response);
});

router.get('/getStores', (request, response) => {
  const appCtrl = getAppCtrl();
  appCtrl.getStores(request, response);
});

router.get('/getCategories', (request, response) => {
  const appCtrl = getAppCtrl();
  appCtrl.getCategories(request, response);
});

router.get('/test', (request, response) => {
  const appCtrl = getAppCtrl();
  appCtrl.test(request, response);
});

export default router;

