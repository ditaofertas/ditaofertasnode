
const getStores = {
  AMERICANAS: 2,
  WALMART: 12,
  SUBMARINO: 22,
};

const getDefaultCategories = () => ([
  'Áudio',
  'Automotivo',
  'Bebês e crianças',
  'Beleza',
  'Brinquedos',
  'Cama mesa e banho',
  'Casa',
  'Eletrodomésticos',
  'Eletroportáteis',
  'Esporte e Lazer',
  'Games',
  'Informática',
  'Instrumentos Musicais',
  'Móveis',
  'Perfumaria e cosméticos',
  'Relógios',
  'TV',
  'Utilidades Domésticas'
]);

const scapeQuot = word => word.replace(/'/g, '"');
const formatWord = word => scapeQuot(word).replace(/[^a-zA-Z\d\s:]/g, '');

const formatURL = url => {
  return !url.startsWith('http://') ? url.replace(/(\/\/)?(w{3})?\.(.*)/g, 'http://www.$3') : url;
};

// https://gist.github.com/marioluan/6923123
const removeAccentuation = word => {
  let string = word.toLowerCase();
  const lettersRegex = {
    a: /[\xE0-\xE6]/g,
    e: /[\xE8-\xEB]/g,
    i: /[\xEC-\xEF]/g,
    o: /[\xF2-\xF6]/g,
    u: /[\xF9-\xFC]/g,
    c: /[\xC7\xE7]/g
  };

  for (const letter in lettersRegex) {
    const regex = lettersRegex[letter];
    string = string.replace(regex, letter);
  }

  return formatWord(string);
};

module.exports = {
  getStores,
  getDefaultCategories,
  formatURL,
  formatWord,
  removeAccentuation,
  scapeQuot
};
