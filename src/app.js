import express from 'express';
import bodyParser from 'body-parser';
import routes from './routes';
import path from 'path';

const app = express();

/*app.set('views', '/views');
app.set('view engine', 'pug');
*/
app.use('/static', express.static('public'));

// O body parser faz com que o que é enviado via método HTTP possa ser interpretado como JSON ou query string, sem ele
// o express indetifica as requisições em formato de texto
app.use(bodyParser.json());
// Faz um deep parser, sem isso apenas o primeiro nível vai ser 'parseado' pra JSON
app.use(bodyParser.urlencoded({extended: true}));
app.use('/', routes);

export default app;
