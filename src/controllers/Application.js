import Promise from 'promise';
import { CrawlerAmericanas, CrawlerWalmart, CrawlerSubmarino } from './../crawlers';
import { getStores } from './../helpers/helper';

/**
 * > saveCategories - Salvar as categorias mineradas
 * > saveOffers(categories, stores) - Recebe do APP as categorias e lojas que as ofertas devem ser pegas e salva no BD
 * > getCategories - Retorna as categorias que aparecerão no APP
 * > getStores - retorna as stores que aparecerão no APP
 */

class Application {
  constructor(DBConnection, CategoriesModel, OffersModel) {
    this.DBConnection = DBConnection;
    this.categoriesModel = new CategoriesModel(DBConnection);
    this.offersModel = new OffersModel(DBConnection);
  }

  static testCron = () => {
    console.log('testCron -- executado...')
  }

  saveCategories = (req, res) => {
    const crawlerAmericanas = new CrawlerAmericanas();
    const crawlerWalmart = new CrawlerWalmart();
    const crawlerSubmarino = new CrawlerSubmarino();
    const americanas = crawlerAmericanas.getCategories();
    const walmart = crawlerWalmart.getCategories();
    const submarino = crawlerSubmarino.getCategories();

    try {
      Promise.all([americanas, walmart])
      .then(response => {
        const categories = [
          ...response[0],
          ...response[1],
        ];

        this.categoriesModel.setCategoriesStore(categories)
          .then(response => res.status(200).json({
            msg: response ? 'Categorias inseridas' : 'Nenhuma categoria inserida'
          }))
          .catch(err => res.status(500).json({msg: err}));
      })
      .catch(err => res.status(500).json({msg: err}));
    } catch (err) {
      res.status(500).json({msg: err});
    }
  }

  saveDefaultCategories = (req, res) => {
    this.categoriesModel.setDefaultCategories()
      .then(response => res.status(200).json(response))
      .catch(err => {
        console.error(err);
        res.status(500).json({msg: err});
      });
  }

  getCrawler = store => {
    switch (store) {
      case getStores.AMERICANAS:
        return new CrawlerAmericanas(this.offersModel);
      default:
        return new CrawlerWalmart(this.offersModel);
    }
  }

  test = (req, res) => {
    const americanas = new CrawlerAmericanas();
    americanas.test();
  }

  saveOffers = async (req, res) => {
    try {
      let categories = [];
      let stores = [];
      const promises = [];
      let categoriesStore = [];

      await this.categoriesModel.getDefaultCategories()
        .then(response => {
          categories = response.map(cat => cat.id);
        })
        .catch(err => console.error(err));

      await this.categoriesModel.getStores()
        .then(response => {
          stores = response.map(str => str.id);
        })
        .catch(err => console.error(err));

      await this.categoriesModel.getStoreCategories(categories, stores)
          .then(response => {
            categoriesStore = response;
          })
          .catch(err => console.error(err));

      /*for (const store of stores) {
        const crawler = this.getCrawler(store);
        const categsStore = categoriesStore.filter(({idStore}) => idStore === store);
        promises.push(crawler.getProducts(categsStore));
      }*/

      const crawler = new CrawlerWalmart(this.offersModel);
      const categsStore = categoriesStore.filter(({idStore}) => idStore === 12);
      promises.push(crawler.getProducts(categsStore));

      Promise.all(promises)
        .then(response => {
          console.log('all---: ', response);
          res.status(200).json({msg: 'Ofertas inseridas'});
        })
        .catch(err => res.status(500).json({msg: err}));
    } catch (err) {
      console.error(err);
      res.status(500).json({msg: err});
    }
  }

  getOffers = (req, res) => {
    const { categories, stores } = req.query;
    if (!categories || !stores) {
      res.status(406).json({msg: 'Necessário informar as categorias e stores'});
    } else {
      const paramCategories = categories.map(cat => parseInt(cat, 10));
      const paramStores = stores.map(store => parseInt(store, 10));

      this.offersModel.getOffers(paramCategories, paramStores)
        .then(response => {
          res.status(200).json({data: response});
        })
        .catch(err => res.status(500).json({err}));
    }
  }

  getStores = (req, res) => {
    this.categoriesModel.getStores()
      .then(response => {
        res.status(200).json({data: response});
      })
      .catch(err => res.status(500).json({err}));
  }

  getCategories = (req, res) => {
    this.categoriesModel.getDefaultCategories()
      .then(response => {
        res.status(200).json({data: response});
      })
      .catch(err => console.error(err));
  }
}

export default Application;
