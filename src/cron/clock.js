import CronJob from 'node-cron';
import ApplicationController from './../controllers/Application';

CronJob.schedule('* * * * *', function(){
  ApplicationController.testCron();
  console.log('executando a cada 1 minuto');
});
