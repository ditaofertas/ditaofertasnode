import phantom from 'phantom';
import request from 'request';
import cheerio from 'cheerio';
import Promise from 'promise';
import fs from 'fs';
import { getStores, formatURL, removeAccentuation, scapeQuot } from './../helpers/helper';

class CrawlerAmericanas {
  constructor(offersModel) {
    this.offersModel = offersModel;
    this.URL = 'http://www.americanas.com.br/todos-os-departamentos?WT.mc_id=home%C2%AD-todos-%C2%ADdepartamentos';
  }

  getCategories = () => new Promise((resolve, reject) => {
    const categories = [];
    request(this.URL, (error, response, body) => {
      if (error) reject(error);
      const $ = cheerio.load(body);
      const $listItems = $('#mapa-do-site dl');
      $listItems.each((index, item) => {
        const categoryName = $(item).find('dt a').text();
        const categoryURL = $(item).find('dt a').attr('href');
        categories.push({
          name: removeAccentuation(categoryName),
          url: formatURL(categoryURL),
          store: getStores.AMERICANAS,
        });
      });
      resolve(categories);
    });
  });

  getProductsList = (html, category) => new Promise((resolve, reject) => {
    const $ = cheerio.load(html);
    const productsContainers = $('[data-component="recommendation"]:not([data-config="department4"])');
    const productsList = [];
    try {
      console.log(`getProductsList ( card-product ) - COUNT: ${$('.card-product').filter((index, product) => {
          return $(product).find('.card-product-name').text().length > 0;
        }).length}`);

      const fileName = `${removeAccentuation(category.categoryAppName).toLowerCase().replace(' ', '-')}.html`;
      console.log('fileName', fileName);
      fs.writeFile(fileName, html, function(err) {
        if(err) {
          return console.log('err', err);
        }
        console.log('Arquivo criado');
      });

      $('.card-product').filter((index, product) => {
        return $(product).find('.card-product-name').text().length > 0 && index < 5;
      }).each((index, product) => {
        console.log('DENTRO DO EACH');

        const name = $(product).find('.card-product-name').text();
        const url = $(product).find('.card-product-url').attr('href');
        const curr = $(product).find('.value:first-child').text();
        const price = $(product).find('.value:last-child').text();
        const prod = {
          name: scapeQuot(name),
          url: url,
          price: `${curr} ${price}`,
          category: category.categoryAppId,
          store: category.idStore
        };

        console.log(`product - ${index} - NAME: [ ${prod.name} ]`);
        productsList.push(prod);
      });

      /*
      productsContainers.each((index, container) => {
        console.log(`EACH [productsContainers] - COUNT: getProductsList ( card-product ) - COUNT: ${$(container).find('.card-product').length}`);
        $(container).find('.card-product').filter((index, product) => {
          return $(product).find('.card-product-name').text().length > 0 && index < 5;
        }).each((index, product) => {
          console.log('DENTRO DO EACH');

          const name = $(product).find('.card-product-name').text();
          const url = $(product).find('.card-product-url').attr('href');
          const curr = $(product).find('.value:first-child').text();
          const price = $(product).find('.value:last-child').text();
          const prod = {
            name: scapeQuot(name),
            url: url,
            price: `${curr} ${price}`,
            category: category.categoryAppId,
            store: category.idStore
          };

          console.log(`product - ${index} - NAME: [ ${prod.name} ]`);
          productsList.push(prod);
        });
      });
      */
      console.log('AMERICANAS - productsList ', productsList.length);
      resolve(productsList);
    } catch (err) {
      console.error('getProductsList [err] ', err);
      reject(err);
    }
  });

  getProductsFromUrl = (ph, page, categories = [], settings = {}, resolve, reject) => {
    console.log('getProductsFromUrl');
    if (!categories.length) {
      console.log('NOT CATEGORIES');
      page.close();
      ph.exit();
      // this.offersModel.destroyConnection();
      resolve({
        code: 200,
        msg: 'Ofertas inseridas',
      });
    } else {
      try {
        const newCategories = [].concat(categories);
        const category = newCategories.pop();
        const _this = this;

        page.open(category.categoryURL, settings).then(status => {
          page.evaluate(function() {
            return document.getElementById('root').innerHTML;
          }).then(function (html) {
            if (!html) {
              console.log('NOT HTML -- ');
              _this.getProductsFromUrl(ph, page, newCategories, settings, resolve, reject);
            } else {
              let productsList = [];
              _this.getProductsList(html, category)
                .then(response => {
                  productsList = response;
                  console.log(`AMERICANAS --- CATEGORY: ID: ${category.categoryAppId} NAME: ${category.categoryAppName}`);

                  _this.offersModel.saveOffers(productsList, false).then(response => {
                    console.log('DB.saveOffers -- then', response);
                    _this.getProductsFromUrl(ph, page, newCategories, settings, resolve, reject);
                  }).catch(err => reject(err));
                });
            }
          });
        });
      } catch (err) {
        console.error(err);
      }
    }
  };

  test = () => {
    let _ph;
    let _page;
    const settings = {
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36',
      'loadImages': false
    };

    phantom.create().then(ph => {
      _ph = ph;
      return _ph.createPage();
    }).then(page => {
      _page = page;
      _ph.viewportSize = {
        width: 1366,
        height: 768
      };

      page.open('http://www.americanas.com.br/categoria/227707?WT.mc_id=mapasite_eletronicos', settings).then(status => {
        page.evaluate(function() {
          return document.getElementById('root').innerHTML;
        }).then(function (html) {
          console.log('TEST - html');
          const $ = cheerio.load(html);
          const productsContainers = $('[data-component="recommendation"]:not([data-config="department4"])');
          const productsList = [];

          productsContainers.each((index, container) => {
            $(container).find('.card-product').filter((index, product) => {
              return $(product).find('.card-product-name').text().length > 0;
            }).slice(0, 10).each((index, product) => {
              const name = $(product).find('.card-product-name').text();
              const url = $(product).find('.card-product-url').attr('href');
              const curr = $(product).find('.value:first-child').text();
              const price = $(product).find('.value:last-child').text();

              productsList.push({
                name: scapeQuot(name),
                url: formatURL(url),
                price: `${curr} ${price}`,
              });
            });
          });

          console.log('AMERICANAS - productsList ', productsList.length);
          // fs.writeFile('page.html', html, function(err){
          //   if(err) {
          //     return console.log('err', err);
          //   }
          //   console.log('Arquivo criado');
          // });
        });
      });
    }).catch(err => reject(err));
  }

  getProducts = (categories = []) => new Promise((resolve, reject) => {
    //console.log('AMERICANAS -- getProducts', categories);
    if (!categories.length) {
      resolve({
        code: 200,
        msg: 'Nenhuma categoria informada',
      });
    } else {
      let _ph;
      let _page;
      const settings = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36',
        'loadImages': false
      };

      phantom.create().then(ph => {
        _ph = ph;
        return _ph.createPage();
      }).then(page => {
        _page = page;
        _ph.viewportSize = {
          width: 1366,
          height: 768
        };

        this.getProductsFromUrl(_ph, _page, categories, settings, resolve, reject);
      }).catch(err => reject(err));
    }
  });

}

export default CrawlerAmericanas;
