import CrawlerAmericanas from './CrawlerAmericanas';
import CrawlerWalmart from './CrawlerWalmart';
import CrawlerSubmarino from './CrawlerSubmarino';

export {
  CrawlerAmericanas,
  CrawlerWalmart,
  CrawlerSubmarino,
};
