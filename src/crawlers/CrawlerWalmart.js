import phantom from 'phantom';
import request from 'request';
import cheerio from 'cheerio';
import Promise from 'promise';
import { getStores, formatURL, removeAccentuation, scapeQuot } from './../helpers/helper';

class CrawlerWalmart {
  constructor(offersModel) {
    this.offersModel = offersModel;
    this.URL = 'https://www.walmart.com.br/';
    this.options = {
      url: this.URL,
      headers: {
        accept: '*/*'
      }
    };
  }

  getCategories = () => new Promise((resolve, reject) => {
    const categories = [];
    request(this.options, (error, response, body) => {
      if (error) reject(error);

      const $ = cheerio.load(body);
      const $listItems = $('.nav-category-bar .nav-category-list .nav-category-content-item');
      $listItems.each((index, item) => {
        const categoryName = $(item).find('.nav-category-item-label').text();
        const categoryURL = $(item).attr('href');

        categories.push({
          name: removeAccentuation(categoryName),
          url: formatURL(categoryURL),
          store: getStores.WALMART,
        });
      });
      resolve(categories);
    });
  });

  getProductsList = (html, category) => new Promise((resolve, reject) => {
    const $ = cheerio.load(html);
    const productsContainers = $('.product-list');
    const productsList = [];

    try {
      productsContainers.each((index, container) => {
        $(container).find('.shelf-product-item').filter((index, product) => {
          return $(product).find('.product-title').text().length > 0;
        }).slice(0, 10).each((index, product) => {
          const name = $(product).find('.product-title').text();
          const priceInt = $(product).find('.price-value .int').text();
          const priceDec = $(product).find('.price-value .dec').text();
          const url = $(product).find('a:first-of-type').attr('href');

          if (name.length > 0 && priceInt.length > 0) {
            productsList.push({
              name: scapeQuot(name),
              url: formatURL(url),
              price: `R$ ${priceInt}${priceDec}`,
              category: category.categoryAppId,
              store: category.idStore
            });
          }
        });
      });

      console.log('WALMART - productsList ', productsList.length);
      resolve(productsList);
    } catch (err) {
      console.error('getProductsList [err] ', err);
      reject(err);
    }
  });

  getProductsFromUrl = (ph, page, categories = [], settings = {}, resolve, reject) => {
    if (!categories.length) {
      page.close();
      ph.exit();
      // this.offersModel.destroyConnection();
      resolve({
        code: 200,
        msg: 'Ofertas inseridas',
      });
    } else {
      try {
        const newCategories = [].concat(categories);
        const category = newCategories.pop();
        const _this = this;

        console.log('CATEGORY', category);

        page.open(category.categoryURL, settings).then(status => {
          page.evaluate(function() {
            return document.getElementById('main').innerHTML;
          }).then(function (html) {
            if (!html) {
              _this.getProductsFromUrl(ph, page, newCategories, settings, resolve, reject);
            } else {
              let productsList = [];
              _this.getProductsList(html, category)
                .then(response => {
                  productsList = response;
                  // console.log(`WALMART --- CATEGORY: ID: ${category.categoryAppId} NAME: ${category.categoryAppName}`);
                  productsList.map(prod => console.log(prod.name))
                  _this.offersModel.saveOffers(productsList, false).then(response => {
                    // console.log('DB.saveOffers -- SAVED');
                    _this.getProductsFromUrl(ph, page, newCategories, settings, resolve, reject);
                  }).catch(err => reject(err));
                });
            }
          });
        });
      } catch (err) {
        console.error('getProductsFromUrl [err] ', err);
      }
    }
  };

  getProducts = (categories = []) => new Promise((resolve, reject) => {
    // console.log('WALMART -- getProducts', categories);
    if (!categories.length) {
      resolve({
        code: 200,
        msg: 'Nenhuma categoria informada',
      });
    } else {
      let _ph;
      let _page;
      const settings = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36',
        'loadImages': false
      };

      phantom.create().then(ph => {
        _ph = ph;
        return _ph.createPage();
      }).then(page => {
        _page = page;
        _ph.viewportSize = {
          width: 1366,
          height: 768
        };

        this.getProductsFromUrl(_ph, _page, categories, settings, resolve, reject);
      }).catch(err => reject(err));
    }
  });
}

export default CrawlerWalmart;
