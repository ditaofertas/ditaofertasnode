import phantom from 'phantom';
import request from 'request';
import cheerio from 'cheerio';
import Promise from 'promise';
import { getStores, formatURL, removeAccentuation, scapeQuot } from './../helpers/helper';

class CrawlerSubmarino {
  constructor(offersModel) {
    this.offersModel = offersModel;
    this.URL = 'https://www.submarino.com.br/mapa-do-site';
    this.options = {
      url: this.URL,
      headers: {
        accept: '*/*'
      }
    };
  }

  getCategories = () => new Promise((resolve, reject) => {
    const categories = [];
    request(this.options, (error, response, body) => {
      if (error) reject(error);

      const $ = cheerio.load(body);
      const $listItems = $('[data-component="sitemapList"] ul.container-level-1 > li');
      $listItems.each((index, item) => {
        const $a = $(item).find('a').first();
        const categoryName = $a.text();
        const categoryURL = $a.attr('href');

        categories.push({
          name: removeAccentuation(categoryName),
          url: `https://www.submarino.com.br${categoryURL}`,
          store: getStores.SUBMARINO,
        });
      });
      resolve(categories);
    });
  });

  getProductsList = (html, category) => new Promise((resolve, reject) => {
    const $ = cheerio.load(html);
    const productsContainers = $('[data-component="recommendation"]');
    const productsList = [];

    try {
      productsContainers.each((index, container) => {
        $(container).find('.card-product-url').filter((index, product) => {
          return $(product).find('.card-product-name').text().length > 0;
        }).slice(0, 10).each((index, product) => {
          const name = $(product).find('.card-product-name').text();
          const price = $(product).find('.value').last().text();
          const url = $(product).attr('href');

          if (name.length > 0 && price.length > 0) {
            productsList.push({
              name: scapeQuot(name),
              url: formatURL(url),
              price: `R$ ${price}`,
              category: category.categoryAppId,
              store: category.idStore
            });
          }
        });
      });

      // console.log('WALMART - productsList ', productsList.length);
      resolve(productsList);
    } catch (err) {
      console.error('getProductsList [err] ', err);
      reject(err);
    }
  });

  getProductsFromUrl = (ph, page, categories = [], settings = {}, resolve, reject) => {
    if (!categories.length) {
      page.close();
      ph.exit();
      // this.offersModel.destroyConnection();
      resolve({
        code: 200,
        msg: 'Ofertas inseridas',
      });
    } else {
      try {
        const newCategories = [].concat(categories);
        const category = newCategories.pop();
        const _this = this;

        page.open(category.categoryURL, settings).then(status => {
          page.evaluate(function() {
            return document.getElementById('root').innerHTML;
          }).then(function (html) {
            if (!html) {
              _this.getProductsFromUrl(ph, page, newCategories, settings, resolve, reject);
            } else {
              let productsList = [];
              _this.getProductsList(html, category)
                .then(response => {
                  productsList = response;
                  // console.log(`WALMART --- CATEGORY: ID: ${category.categoryAppId} NAME: ${category.categoryAppName}`);

                  _this.offersModel.saveOffers(productsList, false).then(response => {
                    // console.log('DB.saveOffers -- SAVED');
                    _this.getProductsFromUrl(ph, page, newCategories, settings, resolve, reject);
                  }).catch(err => reject(err));
                });
            }
          });
        });
      } catch (err) {
        console.error('getProductsFromUrl [err] ', err);
      }
    }
  };

  getProducts = (categories = []) => new Promise((resolve, reject) => {
    // console.log('WALMART -- getProducts', categories);
    if (!categories.length) {
      resolve({
        code: 200,
        msg: 'Nenhuma categoria informada',
      });
    } else {
      let _ph;
      let _page;
      const settings = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36',
        'loadImages': false
      };

      phantom.create().then(ph => {
        _ph = ph;
        return _ph.createPage();
      }).then(page => {
        _page = page;
        _ph.viewportSize = {
          width: 1366,
          height: 768
        };

        this.getProductsFromUrl(_ph, _page, categories, settings, resolve, reject);
      }).catch(err => reject(err));
    }
  });
}

export default CrawlerSubmarino;
