
class Offers {
  constructor(DBConnection) {
    this.conn = DBConnection;
  }

  closeConnection = query => {
    query.on('end', () => {
      this.conn.destroy();
    });
  }

  destroyConnection = () => this.conn.destroy();

  saveOffers = (offers = [], closeConnection = true) => new Promise((resolve, reject) => {
    if (!offers.length) {
      resolve('NO OFFERS');
    } else {
      const insertOffers = offers.map(({name, url, price, category, store}) => (`('${name}', '${url}', '${price}', '${category}', '${store}')`)).join();
      const sqlQuery = `INSERT INTO OFERTAS(dsName, dsURL, dsPrice, idCategory, idStore) VALUES ${insertOffers}`;
      const query = this.conn.query(sqlQuery, (error, results) => {
        if (error) reject(error);
        resolve(results);
      });

      // if (closeConnection) this.closeConnection(query);
    }
  });

  getOffers = (categoriesApp = [], storesApp = []) => new Promise((resolve, reject) => {
    if (!categoriesApp.length || !storesApp.length) {
      reject({msg: 'No categories or stores'});
    } else {
      const categories = categoriesApp.join();
      const stores = storesApp.join();
      const sqlQuery = `
        SELECT off.*,
              cat_app.dsName 'categoryName',
              str.dsName 'storeName'
        FROM ofertas off
        JOIN categorias_app cat_app ON off.idCategory = cat_app.id
        JOIN lojas str ON off.idStore = str.id
        WHERE idCategory IN (${categories}) AND idStore IN (${stores});`;

      const query = this.conn.query(sqlQuery, (error, results) => {
        if (error) reject(error);
        resolve(results);
      });
    }
  });

}

export default Offers;
