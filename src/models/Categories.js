import Promise from 'promise';
import { removeAccentuation } from './../helpers/helper';

class Categories {

  constructor(DBConnection) {
    this.conn = DBConnection;
  }

  closeConnection = query => {
    query.on('end', () => {
      console.log('end');
      this.conn.destroy();
    });
  }

  getStores = () => new Promise((resolve, reject) => {
    const query = this.conn.query('SELECT * FROM LOJAS', (error, results) => {
      if (error) reject(error);
      resolve(results);
    });
  });

  /**
   * Retorna as categorias mineradas dos sites.
   * Se 'store' for passado como parâmetro, serão retornadas categorias específicas.
   * @param {String} store Loja cujas categorias devem ser retornadas
   */
  getCategories = (store, defaultQuery = '', closeConn = true) => new Promise((resolve, reject) => {
    let sqlQuery = defaultQuery;
    if (!defaultQuery) {
      sqlQuery = store ? `SELECT * FROM CATEGORIAS_LOJA WHERE idStore = ${store}` : 'SELECT * FROM CATEGORIAS_LOJA';
    }
    const query = this.conn.query(sqlQuery, (error, results, fields) => {
      if (error) reject(error);
      resolve(results);
    });
  });

  setCategories = (sqlQuery = '', closeConn = true) => new Promise((resolve, reject) => {
    const query = this.conn.query(sqlQuery, (error, results, fields) => {
      if (error) reject(error);
      resolve(results);
    });
  });

  /**
   * Insere as categorias mineradas das lojas na tabela categories_store.
   * @param {String} store Loja a qual as categorias pertencem
   * @param {Array} categories Array com as categorias
   */
  setCategoriesStore = (categories = []) => {
    const insertCategories = categories.map(({name, url, store}) => (`('${name}', '${url}', '${store}')`)).join();
    const sqlQuery = `INSERT INTO CATEGORIAS_LOJA(dsName, dsURL, idStore) VALUES ${insertCategories}`;
    return this.setCategories(sqlQuery);
  };

  /**
   * Insere as categorias mineradas das lojas na tabela CATEGORIAS_APP_store.
   * @param {String} store Loja a qual as categorias pertencem
   * @param {Array} categories Array com as categorias
   */
  setCategoriesAppStore = (categories = [], category) => {
    const insertCategoriesAppStore = categories.map(({id}) => (`('${category.id}', '${id}')`)).join();
    const sqlQuery = `INSERT INTO CATEGORIAS_APP_LOJA(idCategoryApp, idCategoryStore) VALUES ${insertCategoriesAppStore}`;

    return this.setCategories(sqlQuery, false);
  };

  /**
   * Retorna as categorias que aparecerão no APP
   */
  getDefaultCategories = () => new Promise((resolve, reject) => {
    const query = this.conn.query('SELECT * FROM CATEGORIAS_APP', (error, results) => {
      if (error) reject(error);
      resolve(results);
    });
  });

  /**
   * Função recursiva para salvar no banco o relacionamento entre as categorias que aparecerão no App
   * e as categorias mineradas das lojas
   */
  saveCategories = (categoriesList = [], resolve, reject) => {
    if (!categoriesList.length) {
      resolve({
        code: 200,
        msg: 'Categorias inseridas'
      });
    } else {
      const newCategoriesList = [].concat(categoriesList);
      const category = newCategoriesList.pop();
      const sqlQuery = `SELECT id, dsName, idStore FROM CATEGORIAS_LOJA WHERE dsName LIKE '%${category.dsName}%'`;

      this.getCategories(null, sqlQuery, false)
        .then(response => {
          try {
            if (response.length) {
              this.setCategoriesAppStore(response, category)
                .then(response => {
                  this.saveCategories(newCategoriesList, resolve, reject);
                }).catch(err => reject(err));
            } else {
              this.saveCategories(newCategoriesList, resolve, reject);
            }
          } catch (err) {
            console.log(err);
          }
        }).catch(err => reject(err));
    }
  }

  /**
   * Função para salvar no banco o relacionamento entre as categorias que aparecerão no app e as mineradas
   */
  setDefaultCategories = () => new Promise((resolve, reject) => {
    this.getDefaultCategories()
      .then(response => {
        try {
          const categoriesApp = response.map(category => ({
            ...category,
            dsName: removeAccentuation(category.dsName)
          }));

          this.saveCategories(categoriesApp, resolve, reject);
        } catch (error) {
          console.log('error', error);
        }
      })
      .catch(err => reject(err));
  });

  /**
   * Retorna as categorias das lojas que correspondem as categorias passadas pelo APP
   */
  getStoreCategories = (categoriesApp = [], storesApp = []) => new Promise((resolve, reject) => {
    if (!categoriesApp.length) {
      resolve({
        code: 200,
        msg: 'No categories'
      });
    } else {
      const categories = categoriesApp.join();
      const stores = storesApp.join();
      const sqlQuery = `
        SELECT cat_str.id,
             cat_app.id 'categoryAppId',
             cat_app.dsName 'categoryAppName',
             cat_str.dsURL 'categoryURL',
             str.dsName,
             cat_str.idStore
        FROM categorias_loja cat_str
        JOIN categorias_app_loja cat_app_str ON cat_app_str.idCategoryStore = cat_str.id
        JOIN categorias_app cat_app ON cat_app_str.idCategoryApp = cat_app.id
        JOIN lojas str ON str.id = cat_str.idStore
        WHERE cat_app_str.idCategoryApp IN (${categories}) AND cat_str.idStore IN (${stores});`;

      const query = this.conn.query(sqlQuery, (error, results) => {
        if (error) reject(error);
        resolve(results);
      });
    }
  });

}

export default Categories;
